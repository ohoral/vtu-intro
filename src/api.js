const REQUESTS_TYPE_URL =
  "https://www.mockachino.com/355f8bc9-ce47-42/requests";
const SUBMIT_URL = "https://www.mockachino.com/6baa6216-8873-40/apply";

export const getRequestTypes = async () => {
  const data = await fetch(REQUESTS_TYPE_URL);
  const { requests } = await data.json();
  return requests;
};

export const submitRequest = async (data) => {
  const response = await fetch(SUBMIT_URL, {
    method: "POST",
    mode: "cors",
    body: JSON.stringify(data), // body data type must match "Content-Type" header
  });

  const { status } = await response.json();

  return status;
};
